const icons = {
	'01d': require ("../assets/icons/01d.png"),
	'01n': require("../assets/icons/01n.png"),
	'02d': require("../assets/icons/02d.png"),
	'02n': require("../assets/icons/02n.png"),
	'03d': require("../assets/icons/03d.png"),
	'03n': require("../assets/icons/03n.png"),
	'04d': require("../assets/icons/04d.png"),
	'04n': require("../assets/icons/04n.png"),
	'09d': require("../assets/icons/09d.png"),
	'09n': require("../assets/icons/09n.png"),
	'10d': require("../assets/icons/10d.png"),
	'10n': require("../assets/icons/10n.png"),
	'11d': require("../assets/icons/11d.png"),
	'11n': require("../assets/icons/11n.png"),
	'13d': require("../assets/icons/13d.png"),
	'13n': require("../assets/icons/13n.png"),
	'50d': require("../assets/icons/50d.png"),
	'50n': require("../assets/icons/50n.png"),
	'wind': require("../assets/icons/wind.png"),
	'humidity': require("../assets/icons/humidity.png"),
	'coloredIcon': require("../assets/icons/coloredIcon.png"),
	'mapIcon': require("../assets/icons/mapIcon.png"),
	'windx': require("../assets/icons/windx.gif"),
	'globe': require("../assets/icons/globe.gif"),
	'sun': require("../assets/icons/sun.gif"),
	'suny': require("../assets/icons/suny.gif"),
}


const backgrounds = {
	'01d': require("../assets/backgrounds/01d.png"),
	'01n': require("../assets/backgrounds/01n.png"),
	'02d': require("../assets/backgrounds/02d.png"),
	'02dr': require("../assets/backgrounds/02dr.png"),
	'02dt': require("../assets/backgrounds/02dt.png"),
	'02n': require("../assets/backgrounds/02n.png"),
	'03dr': require("../assets/backgrounds/03dr.png"),
	'04d': require("../assets/backgrounds/04d.png"),
	'09d': require("../assets/backgrounds/09d.png"),
	'09nb': require("../assets/backgrounds/09nb.png"),
	'09n': require("../assets/backgrounds/09n.png"),
	'10n': require("../assets/backgrounds/10n.png"),
	'11n': require("../assets/backgrounds/11n.png"),
	'13d': require("../assets/backgrounds/13d.png"),
	'13n': require("../assets/backgrounds/13n.png"),
	'16n': require("../assets/backgrounds/16n.png"),
	'50n': require("../assets/backgrounds/50n.png")

}

export { icons , backgrounds }