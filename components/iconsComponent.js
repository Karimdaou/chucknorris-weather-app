import React, {useState, useEffect, useCallback } from 'react';
import { Alert,
    ImageBackground,
    Image,
    StyleSheet,
    Text,
    View, 
    Dimensions,
    TextInput,
    ScrollView,
    TouchableOpacity,
    AsyncStorage,
    FlatList,
    Picker} from 'react-native';
import {icons , backgrounds} from '../components/images.js'


    export default class IconsComponent extends React.Component {

        render(){
            return (
             <View style={{ flex:1, height:'100%',flexDirection:'row', justifyContent:'space-between', paddingHorizontal:'10%',paddingTop:'5%' }}>
             <TouchableOpacity onPress={(arg)=>this.props.displayIconsx(true)}><Image  style={{ height:40,width:40}} source={icons['mapIcon']}/></TouchableOpacity>
             <TouchableOpacity onPress={()=>this.props.changeLoc2()}><Image style={{ height:40,width:40}} source={icons['coloredIcon']}/></TouchableOpacity>
           </View>
        );}}