import React, {useState, useEffect, useCallback } from 'react';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';


export default class GooglePlacesInput extends React.Component {


    state = {}


    render(){
    return (
      <GooglePlacesAutocomplete
        placeholder='Search'
        minLength={2}
        autoFocus={false}
        returnKeyType={'search'}
        
        listViewDisplayed='false'
        fetchDetails={true}
        renderDescription={(row) => row.description}
        onPress={(data, details = null) => {
                this.props.changeLoc(details)
                this.props.displayIconsx(false)
        }}
        getDefaultValue={() => {
          return ''; 
        }}
        query={{
          key: 'AIzaSyDXB0Jeep78Zz6gVa5FXpa4j3tP3fQWvh0',
          language: 'en', 
          types: '(cities)'
        }}
        styles={{
      textInputContainer: {
        borderTopWidth: 0,
        borderBottomWidth:0,
        width:this.props.width,
        height:60,
        backgroundColor:'white'
      },
      textInput: {
        marginLeft: 0,
        marginRight: 0,
        height: 38,
        color: '#5d5d5d',
        fontSize: 16,
        backgroundColor:'#ddd',
      },
      predefinedPlacesDescription: {
        color: 'white'
      },
    }}
   
        currentLocation={false}
        currentLocationLabel="Current location"
        nearbyPlacesAPI='GooglePlacesSearch'
        
      />
    );
  }
  }
 
  