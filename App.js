import React, {useState, useEffect, useCallback } from 'react';
import { Alert,
        ImageBackground,
        Image,
        StyleSheet,
        Text,
        View, 
        Dimensions,
        TextInput,
        ScrollView,
        TouchableOpacity,
        AsyncStorage,
        FlatList,
        Picker} from 'react-native';
import {
          LineChart,
          BarChart,
          PieChart,
          ProgressChart,
          ContributionGraph,
          StackedBarChart
        } from "react-native-chart-kit";
import { Button,
          LinearGradient, 
          Input, 
          Divider,
          Icon,
          Theme } from 'react-native-elements';                
import useStateWithCallback from "use-state-with-callback";
import Axios from 'axios';
import {icons , backgrounds} from './components/images.js'
import PercentageCircle from 'react-native-percentage-circle';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import GooglePlacesInput from './components/locationFinder.js'
import IconsComponent from './components/iconsComponent.js'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";


const { height,width } = Dimensions.get('window')
const homePlace = { description: 'Home', geometry: { location: { lat: 48.8152937, lng: 2.4597668 } }};
const workPlace = { description: 'Work', geometry: { location: { lat: 48.8496818, lng: 2.2940881 } }};

export default function App(props) {


  const [coords,setCoords] = useStateWithCallback(null, coord =>{
    if (coord != null && count != 1)
    {
      setCount(count + 1)
      weatherLocationDaily()
      
    }
  })

  const [weatherToday,setWeatherToday] = useStateWithCallback(null, weatherTodayx =>{
    if (weatherTodayx != null && county != 1  )
    {
      weatherLocationHourly()
       setCounty(county + 1)
       
       console.log(weatherToday,'weather today')
    }
  })

  const [weatherHourly,setWeatherHourly] = useStateWithCallback(null, weatherHourlyx =>{
    if (weatherHourlyx != null && countz != 1 )
    {
      weatherLocationWeek()
       setCountz(countz + 1)
       
    }
  })

  const [weatherWeek,setWeatherWeek] = useStateWithCallback(null, weatherWeekx =>{
    if (weatherWeekx != null && countq != 1  )
    {
       getQuote()
       setCountq(countq + 1)
       

    }
  })  

  const [quote,setQuote] = useStateWithCallback(null, quotex =>{
    if (quotex != null && count1 != 1  )
    {
      displayDaysx()
      setCount1(count1 + 1)
      
    }
  }) 

  const [daysx,setDaysx] = useStateWithCallback(null, daysxx =>{
    if (daysxx != null && countw != 1 )
    {
      displayDaysy()
      setCountw(countw + 1)
      
    }
  })

  const [daysy,setDaysy] = useStateWithCallback(null, daysyy =>{
    if (daysyy != null && counto != 1 )
    {
      getHour()
      getIconsx()
      setCounto(counto + 1)
      
    }
  })

  const [hours,setHours] = useStateWithCallback(null, hours =>{
    if (hours != null && countx != 1  )
    
    {
      getIconsx()
      setLoader(false)
      setCountx(countx + 1)
    }
  })

  const [iconx,setIconx] = useStateWithCallback(null, iconx =>{
    if (iconx != null && countr != 1 )
    {
      setLoader(false)
      setCountr(countr + 1)
      
    }
  })

  const [backgroundColorX, setBackgroundColorX] = useState('')
  const [loader, setLoader] = useState(true)
  const [loaderx, setLoaderx] = useState(false)
  const [count, setCount] = useState(0)
  const [countx, setCountx] = useState(0)
  const [county, setCounty] = useState(0)
  const [countz, setCountz] = useState(0)
  const [countq, setCountq] = useState(0)
  const [countw, setCountw] = useState(0)
  const [countr, setCountr] = useState(0)
  const [counto, setCounto] = useState(0)
  const [count1, setCount1] = useState(0)
  const [fontcolorx, setFontcolorx] = useState('black')
  const [sunTime,setSunTime] = useState({sunrise:2,sunset:2})
  const [backgroundx,setBackgroundx] = useState('')


useEffect(()=>{

  getCoords()
},[])

const getCoords =  () => {
    navigator.geolocation.getCurrentPosition(  (position)=>{
        
        var key = 'yourkey'
        setCoords({...position})
    },
    error => Alert.alert(error.message),
    { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    )
  }

const getHour = () => {

    var hoursx = new Date((weatherToday.dt+weatherToday.timezone)*1000).toString();
    var datex= hoursx.split(' ')[4]
    var tempDay= datex.substring(0,2)
    setHours(Number(tempDay))
    setCountx(0)
    var sunrise = new Date((weatherToday.sys.sunrise+weatherToday.timezone)*1000).toString();
    var sunrisex= sunrise.split(' ')[4]
    var sunrisey= sunrisex.substring(0,5)
    var sunset = new Date((weatherToday.sys.sunset+weatherToday.timezone)*1000).toString();
    var sunsetx= sunset.split(' ')[4]
    var sunsety= sunsetx.substring(0,5)
    var tempx = {sunset:sunsety,sunrise:sunrisey}
    setSunTime({...tempx})

  }

const weatherLocationDaily = async () => {
  var lat = coords.coords.latitude
  var lon = coords.coords.longitude
  var key = 'yourkey'
    try {
      const response= await Axios.post(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&units=metric&APPID=${key}`)
      setWeatherToday({...response.data})
      setCounty(0)
    }
      catch(error) 
    {
      console.log(error)
    }
}

const weatherLocationHourly = async () => {
  var lat = coords.coords.latitude
  var lon = coords.coords.longitude
  var key = 'yourkey'
    try {
      const response1= await Axios.post(`http://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&units=metric&APPID=${key}`)
      setWeatherHourly({...response1.data})
      setCountz(0)

    }
      catch(error) 
    {
      console.log(error)
    }
}

const weatherLocationWeek  =  async () => {
  var lat = coords.coords.latitude
  var lon = coords.coords.longitude
  var key = 'yourkey'
  var cnt = 7
    try {
      const response2= await Axios.post(`http://api.openweathermap.org/data/2.5/forecast/daily?lat=${lat}&lon=${lon}&cnt=${cnt}&units=metric&APPID=${key}`)
      setWeatherWeek({...response2.data})
      setCountq(0)
    }
      catch(error) 
    {
      console.log(error)
    }
}

const getQuote  =  async () => {

    try {
      const response2= await Axios.get(`https://api.chucknorris.io/jokes/random`)
      setQuote({...response2.data})
      setCount1(0)
    }
      catch(error) 
    {
      console.log(error)
    }
}


const displayDaysx = () => {
  let tempDays = []
  weatherWeek.list.forEach((day, i) => {
    let tempDay
    let tempIcon
    let tempTemp
    let date = new Date(day.dt*1000).toString()
    tempDay = date.split(' ')[0]
    tempIcon = icons[weatherWeek.list[i].weather[0].icon]
    tempMax = day.temp.max
    tempMin = day.temp.min
    if(tempDays.length < 6) {
      tempDays.push({name: tempDay, icon: tempIcon, tempMax: tempMax, tempMin:tempMin})
    }
  })
  tempDays.shift()
  setDaysx([...tempDays])
  setCountw(0)
}

const displayDaysy = () => {
  let tempDays = []
  let timeShift = weatherHourly.city.timezone
  weatherHourly.list.forEach((day, i) => {
    let tempDay
    let tempIcon
    let tempTemp
    let d = new Date().toString()
    let date = new Date((day.dt+timeShift)*1000).toString()
    datex= date.split(' ')[4]
    tempDay= datex.substring(0,5)
    if (tempDay == '00:00'){tempDayx = '12AM'}
    else if (tempDay == '01:00'){tempDayx = '1AM'}
    else if (tempDay == '02:00'){tempDayx = '2AM'}
    else if (tempDay == '03:00'){tempDayx = '3AM'}
    else if (tempDay == '04:00'){tempDayx = '4AM'}
    else if (tempDay == '05:00'){tempDayx = '5AM'}
    else if (tempDay == '06:00'){tempDayx = '6AM'}
    else if (tempDay == '07:00'){tempDayx = '7AM'}
    else if (tempDay == '08:00'){tempDayx = '8AM'}
    else if (tempDay == '09:00'){tempDayx = '9AM'}
    else if (tempDay == '10:00'){tempDayx = '10AM'}
    else if (tempDay == '11:00'){tempDayx = '11AM'}
    else if (tempDay == '12:00'){tempDayx = '12PM'}
    else if (tempDay == '13:00'){tempDayx = '1PM'}
    else if (tempDay == '14:00'){tempDayx = '2PM'}
    else if (tempDay == '15:00'){tempDayx = '3PM'}
    else if (tempDay == '16:00'){tempDayx = '4PM'}
    else if (tempDay == '17:00'){tempDayx = '5PM'}
    else if (tempDay == '18:00'){tempDayx = '6PM'}
    else if (tempDay == '19:00'){tempDayx = '7PM'}
    else if (tempDay == '20:00'){tempDayx = '8PM'}
    else if (tempDay == '21:00'){tempDayx = '9PM'}
    else if (tempDay == '22:00'){tempDayx = '10PM'}
    else if (tempDay == '23:00'){tempDayx = '11PM'}
    tempIcon = icons[weatherHourly.list[i].weather[0].icon]
    tempTemp = weatherHourly.list[i].main.temp
    if(tempDays.length < 6) {
      tempDays.push({name: tempDayx, icon: tempIcon, temp: tempTemp})
    }
  })
  tempDays.shift()
  setDaysy([...tempDays])
  setCounto(0)
}

var getIconsx = () => {
    if (
            hours <= 8 && weatherToday.weather[0].main == 'Clear' && weatherToday.main.temp >=0 ||
            hours >=18 && weatherToday.weather[0].main == 'Clear' && weatherToday.main.temp >=0 )
            {
              setBackgroundx(backgrounds['50n']),
              setBackgroundColorX('#232323')
              setIconx(icons['02n'])
              setFontcolorx('white')
              setLoader(false)
            }
    else if (
            hours >= 8 && weatherToday.weather[0].main == 'Clear' && weatherToday.main.temp >=0  ||
            hours <=18 && weatherToday.weather[0].main == 'Clear' && weatherToday.main.temp >=0 )
            {
              setBackgroundx(backgrounds['02dt']),
              setBackgroundColorX('#4A512F')
              setIconx(icons['02d'])
              setFontcolorx('black')
              setLoader(false)
            }
    else if (
            hours <= 8 && weatherToday.weather[0].main == 'Clouds' && weatherToday.main.temp >=0  ||
            hours >=18 && weatherToday.weather[0].main == 'Clouds' && weatherToday.main.temp >=0 )
            {
              setBackgroundx(backgrounds['50n']),
              setBackgroundColorX('#232323')
              setIconx(icons['04n'])
              setFontcolorx('white')
              setLoader(false)
            }
    else if (
            hours >= 8 && weatherToday.weather[0].main == 'Clouds' && weatherToday.main.temp >=0  ||
            hours <=18 && weatherToday.weather[0].main == 'Clouds' && weatherToday.main.temp >=0 )
            {
              setBackgroundx(backgrounds['09n']),
              setBackgroundColorX('#4A5229')
              setIconx(icons['04d'])
              setFontcolorx('black')
              setLoader(false)
            }
    else if (
            hours >= 8 && weatherToday.weather[0].main == 'Rain' && weatherToday.main.temp >=0  ||
            hours <=18 && weatherToday.weather[0].main == 'Rain' && weatherToday.main.temp >=0 )
            {
              setBackgroundx(backgrounds['02dr']),
              setBackgroundColorX('#4A512E')
              setIconx(icons['09d'])
              setFontcolorx('black')
              setLoader(false)
            }   
    else if (
            hours <= 8 && weatherToday.weather[0].main == 'Rain' && weatherToday.main.temp >=0  ||
            hours >=18 && weatherToday.weather[0].main == 'Rain' && weatherToday.main.temp >=0 )
            {
              setBackgroundx(backgrounds['10n']),
              setBackgroundColorX('#4A522D')
              setIconx(icons['09n'])
              setFontcolorx('white')
              setLoader(false)
            }
    else if (
            hours <= 8 &&  weatherToday.main.temp <=0  ||
            hours >=18 &&  weatherToday.main.temp <=0  ||
            hours <= 8 && weatherToday.weather[0].main == 'Snow' ||
            hours >=18 && weatherToday.weather[0].main == 'Snow' )
            {
              setBackgroundx(backgrounds['13n']),
              setBackgroundColorX('#001827')
              setIconx(icons['13d'])
              setFontcolorx('white')
              setLoader(false)
            }
    else if (
            hours >= 8 && weatherToday.main.temp <=0  ||
            hours <=18 && weatherToday.main.temp <=0  ||
            hours >= 8 && weatherToday.weather[0].main == 'Snow' ||
            hours <=18 && weatherToday.weather[0].main == 'Snow' )
            {
              setBackgroundx(backgrounds['13d']),
              setBackgroundColorX('#001827')
              setIconx(icons['13d'])
              setFontcolorx('black')
              setLoader(false)
            }
    else if (
            hours >= 8 && weatherToday.weather[0].main == 'ThunderStorm' && weatherToday.main.temp >=0  ||
            hours <=18 && weatherToday.weather[0].main == 'ThunderStorm' && weatherToday.main.temp >=0 )
            {
              setBackgroundx(backgrounds['01n']),
              setBackgroundColorX('#0A2832')
              setIconx(icons['11d'])
              setLoader(false)
            }   
    else if (
            hours <= 8 && weatherToday.weather[0].main == 'Thunderstorm' && weatherToday.main.temp >=0  ||
            hours >=18 && weatherToday.weather[0].main == 'Thunderstorm' && weatherToday.main.temp >=0 )
            {
              setBackgroundx(backgrounds['16n']),
              setBackgroundColorX('#1C1C1C')
              setIconx(icons['11n'])
              setFontcolorx('white')
              setLoader(false)
            }  
            setCountr(0)                  
  }
  
var changeLoc = (details) => {
    var temp = coords
    setLoader(true)
    temp.coords.latitude = details.geometry.location.lat
    temp.coords.longitude = details.geometry.location.lng
    setCoords({...temp})
    setCount(0)
    }

var changeLoc2 = () => {
      setLoader(true)
      getCoords()
      setCount(0)
      }


var displayIconsx = (arg) => {
     setLoaderx(arg)
    };
  

  return (
    loader ? null
    : <View style={{
      backgroundColor: backgroundColorX,
      alignItems: 'center',
      justifyContent: 'flex-start',
    }}>

      { loaderx ? <View style={{paddingTop:'10%', backgroundColor:'white'}}>
                    <GooglePlacesInput changeLoc={changeLoc} width={width} height={height} displayIconsx={displayIconsx}></GooglePlacesInput>
                    <View style={{position:'absolute',top:'37%',left:'22%'}}><Image style={{color:'white', height:200, width:200}}source={icons['globe']}></Image></View>
                    </View> : null}
      <ScrollView>
        <View style={{height: height, backgroundColor:'red',flexDirection:'column'}}>
        <ImageBackground source={backgroundx} style={{width: '100%', height: '100%',flex:1}}>
          <View style={{position:'absolute', top:'2%', width:'100%', zIndex:5}}><IconsComponent displayIconsx={displayIconsx} changeLoc2={changeLoc2}></IconsComponent></View>

      <View style={{ flexDirection:'row', height: '60%'}}>


        <View style={{width:'60%',height:'100%', flexDirection:'column',flex:1}}>
        <View style={{width:'100%',marginTop:'80%',paddingTop:'30%'}}>
          <Text style={{ fontFamily:'monospace', fontSize: RFValue(15,height), textAlign:'center',color: fontcolorx}}>{quote.value}</Text>

        </View></View>





        <View style={{width:'40%',height:'100%'}}>
        <View style={{zIndex:1,width:'100%',height:'100%', paddingTop: '10%', justifyContent:'space-evenly', alignItems:'center',flexDirection:'column'}}>
          <View>
          <Image style={{height: 150, width: 150}} source={iconx}/>
          </View>
          <View style={{ flex: 1, alignItems: 'center'}}>
          <View style={{justifyContent:'flex-start',flex:1}}>
          <Text style={{ fontFamily:'monospace', textAlign:'center',color: fontcolorx, fontSize: RFValue(25,height)}}>{weatherToday.name}</Text>
          <Text style={{ fontFamily:'monospace', textAlign:'center', fontSize: 30,color: fontcolorx, fontSize: RFValue(23,height)}}>{Math.round(weatherToday.main.temp)}°C</Text>
          </View>
          <View style={{justifyContent:'flex-end',flex:1}} >
          <Text style={{ fontFamily:'monospace', textAlign:'center',color: fontcolorx, fontSize: RFValue(12,height)}}>Feels Like</Text>
          <Text style={{ fontFamily:'monospace', textAlign:'center',color: fontcolorx, fontSize: RFValue(15,height)}}>{Math.round(weatherToday.main.feels_like)}°C</Text>
          </View>
        </View></View>

        </View>




        
      </View>

      
      <View style={{height:'100%', width:'100%', flex:5,flexDirection:'row',paddingTop:'20%'}}>
      <View style={{height:'100%',width:'30%'}}></View>   
      <View style={{height: '100%',width: '70%', flexDirection: 'row', justifyContent: 'space-evenly'}}>
        {
        daysy.map((ele, i) => <View style={{paddingTop:'5%'}}>
                                <Text style={{color: fontcolorx, fontSize: RFValue(12,height)}}>{ele.name}</Text>
                                <Text style={{color: fontcolorx, fontSize: RFValue(12,height)}}>{Math.round(ele.temp)}°C</Text>
                                <Image style={{height: 40, width: 40}} source={ele.icon}/>
                              </View>
          )
        }
      </View>
      
      </View>
      </ImageBackground>
        </View>
        <View>
      <View style = {{flexDirection: 'column',justifyContent: 'space-evenly',}}>
        {
        daysx.map((ele, i) => {
                             return <View style={{flexDirection: 'row',justifyContent: 'space-between',borderTopColor:'white',borderTopWidth: 2,paddingHorizontal:'2%'}}>
                                <Text style={{color:'white', fontSize: RFValue(18,height)}}>{ele.name}</Text>
                                <Image style={{height: 50, width: 50}} source={ele.icon}/>
                                <Text style={{color:'white', fontSize: RFValue(18,height)}}>{Math.round(ele.tempMax)}/{Math.round(ele.tempMin)}°C</Text>
                                
                              </View>}
          )
        }
      </View>
      <View style={{borderTopColor:'white', borderTopWidth:2, paddingVertical:'5%', flexDirection:'row'}}>
        <View style={{width:'50%',alignItems:'center', alignSelf:'center'}}>
          <Image style={{color:'white', height:20, width:20}}source={icons['humidity']}></Image>
        <Text style={{color:'white' , fontWeight:'bold', fontSize: RFValue(20,height)}}>Humidity:</Text>
        </View>
        <View style={{width:'50%', paddingLeft:'12%'}}>
        <PercentageCircle radius={50} percent={weatherToday.main.humidity} color={'white'} bgcolor={'grey'} innerColor={'#69CCEB'}>
          </PercentageCircle>
          </View>
          </View>
      <View style={{borderTopColor:'white', borderTopWidth:2, paddingVertical:'7%', flexDirection:'row'}}>
        <View style={{width:'50%',alignItems:'center', alignSelf:'center'}}>
          <Image style={{color:'white', height:30, width:30}}source={icons['wind']}></Image>
        <Text style={{color:'white' , fontWeight:'bold', fontSize: RFValue(20,height)}}>Windspeed:</Text>
        </View>
        <View style={{width:'50%', paddingLeft:'5%',alignItems:'center',}}>
        <Image style={{color:'white', height:50, width:50,paddingRight:'15%'}}source={icons['windx']}></Image>
        <Text style={{color:'white' , fontWeight:'bold', fontSize: RFValue(12,height),paddingRight:'15%'}}>{weatherToday.wind.speed} KM/H</Text>
          </View>
          </View>

          
        <View style={{borderTopColor:'white', borderTopWidth:2, paddingVertical:'2%', flexDirection:'row',paddingHorizontal:'3%'}}>
        <View style={{width:'100%',alignItems:'center', alignSelf:'center',flexDirection:'row',justifyContent:'space-between',paddingHorizontal:'3%'}}>
        <View style={{flexDirection:'column',alignItems:'center'}}>
            <View><Text style={{color:'white' , fontWeight:'bold', fontSize: RFValue(12,height)}}>Sunrise:</Text></View>
            <View><Text style={{color:'white' , fontWeight:'bold', fontSize: RFValue(12,height)}}>{sunTime.sunrise}</Text></View>
            </View>
          <Image style={{color:'white', height:100, width:100}}source={icons['suny']}></Image>
          <View style={{flexDirection:'column',alignItems:'center'}}>
            <View><Text style={{color:'white' , fontWeight:'bold', fontSize: RFValue(12,height)}}>Sunset:</Text></View>
            <View><Text style={{color:'white' , fontWeight:'bold', fontSize: RFValue(12,height)}}>{sunTime.sunset}</Text></View>
            </View>
        </View></View>
        </View>
      </ScrollView> 
    </View>
  );
}

